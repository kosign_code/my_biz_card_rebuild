package com.vannet.mybizcard_rebuild.model;

public class CardItem {
    private String CARD_CORP_CD;
    private String CARD_NO;
    private String CARD_ORG_NM;
    private String BIZ_NM;
    private String CARD_NICK_NM;
    private String EXPR_TRM ;
    private String CARD_GB;
    private String SCRP_YN;
    private String SEQ_NO ;

    public CardItem(String CARD_CORP_CD, String CARD_NO, String CARD_ORG_NM, String BIZ_NM,
           String CARD_NICK_NM, String EXPR_TRM, String CARD_GB, String SCRP_YN, String SEQ_NO)
    {
        this.CARD_CORP_CD = CARD_CORP_CD;
        this.CARD_NO = CARD_NO;
        this.CARD_ORG_NM = CARD_ORG_NM;
        this.BIZ_NM = BIZ_NM;
        this.CARD_NICK_NM = CARD_NICK_NM;
        this.EXPR_TRM = EXPR_TRM;
        this.CARD_GB = CARD_GB;
        this.SCRP_YN = SCRP_YN;
        this.SEQ_NO = SEQ_NO;
    }

    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }

    public String getBIZ_NM() {
        return BIZ_NM;
    }

    public void setBIZ_NM(String BIZ_NM) {
        this.BIZ_NM = BIZ_NM;
    }

    public String getCARD_NICK_NM() {
        return CARD_NICK_NM;
    }

    public void setCARD_NICK_NM(String CARD_NICK_NM) {
        this.CARD_NICK_NM = CARD_NICK_NM;
    }

    public String getEXPR_TRM() {
        return EXPR_TRM;
    }

    public void setEXPR_TRM(String EXPR_TRM) {
        this.EXPR_TRM = EXPR_TRM;
    }

    public String getCARD_GB() {
        return CARD_GB;
    }

    public void setCARD_GB(String CARD_GB) {
        this.CARD_GB = CARD_GB;
    }

    public String getSCRP_YN() {
        return SCRP_YN;
    }

    public void setSCRP_YN(String SCRP_YN) {
        this.SCRP_YN = SCRP_YN;
    }

    public String getSEQ_NO() {
        return SEQ_NO;
    }

    public void setSEQ_NO(String SEQ_NO) {
        this.SEQ_NO = SEQ_NO;
    }
}

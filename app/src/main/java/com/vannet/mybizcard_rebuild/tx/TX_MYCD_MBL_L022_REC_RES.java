package com.vannet.mybizcard_rebuild.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

public class TX_MYCD_MBL_L022_REC_RES extends TxMessage {
    public static final String TXNO = "MYCD_MBL_L022";
    private TX_MYCD_MBL_L022_REC_RES_DATA mTxKeyData;
    public TX_MYCD_MBL_L022_REC_RES(Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L022_REC_RES_DATA();
        super.initRecvMessage(obj);
    }

    private class TX_MYCD_MBL_L022_REC_RES_DATA{
        private String CARD_CORP_CD = "CARD_CORP_CD";
        private String CARD_NO = "CARD_NO";
        private String CARD_ORG_NM = "CARD_ORG_NM";
        private String BIZ_NM = "BIZ_NM";
        private String CARD_NICK_NM = "CARD_NICK_NM";
        private String EXPR_TRM = "EXPR_TRM";
        private String CARD_GB = "CARD_GB";
        private String SCRP_YN = "SCRP_YN";
        private String SEQ_NO = "SEQ_NO";

    }

    public String getCARD_CORP_CD() throws JSONException{
        return getString(mTxKeyData.CARD_CORP_CD);
    }
    public String getCARD_NO() throws JSONException{
        return getString(mTxKeyData.CARD_NO);
    }
    public String getCARD_ORG_NM() throws JSONException{
        return getString(mTxKeyData.CARD_ORG_NM);
    }
    public String getBIZ_NM() throws JSONException{
        return getString(mTxKeyData.BIZ_NM);
    }
    public String getCARD_NICK_NM() throws JSONException{
        return getString(mTxKeyData.CARD_NICK_NM);
    }
    public String getEXPR_TRM() throws JSONException{
        return getString(mTxKeyData.EXPR_TRM);
    }
    public String getCARD_GB() throws JSONException{
        return getString(mTxKeyData.CARD_GB);
    }
    public String getSCRP_YN() throws JSONException{
        return getString(mTxKeyData.SCRP_YN);
    }
    public String getSEQ_NO() throws JSONException{
        return getString(mTxKeyData.SEQ_NO);
    }
}

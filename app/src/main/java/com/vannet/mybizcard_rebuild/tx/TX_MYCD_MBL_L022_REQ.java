package com.vannet.mybizcard_rebuild.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

public class TX_MYCD_MBL_L022_REQ extends TxMessage {
    public static final String TXNO = "MYCD_MBL_L022";
    private TX_MYCD_MBL_L022_REQ_DATA mTxKeyData;

    public TX_MYCD_MBL_L022_REQ() throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L022_REQ_DATA();
        super.initSendMessage();
    }

    public void setINQ_DT(String value) throws JSONException{
        mSendMessage.put(mTxKeyData.INQ_DT, value);
    }

    private class TX_MYCD_MBL_L022_REQ_DATA{
        private String INQ_DT = "INQ_DT";
    }
}

package com.vannet.mybizcard_rebuild.tx;

import com.webcash.sws.network.tx.TxMessage;

import org.json.JSONException;

public class TX_MYCD_MBL_L022_RES extends TxMessage {
    public static final String TXNO= "MYCD_MBL_R002";
    private TX_MYCD_MBL_L022_RES_DATA mTxKeyData;
    public TX_MYCD_MBL_L022_RES (Object obj) throws Exception{
        mTxKeyData = new TX_MYCD_MBL_L022_RES_DATA();
        super.initRecvMessage(obj);
    }


    public String getCARD_REG_CNT() throws JSONException{
        return getString(mTxKeyData.CARD_REG_CNT);
    }

    public String getCARD_MAGR_NM() throws JSONException{
        return getString(mTxKeyData.CARD_MAGR_NM);
    }

    public TX_MYCD_MBL_L022_REC_RES getREC () throws Exception{
        return new TX_MYCD_MBL_L022_REC_RES(getRecord(mTxKeyData.REC));
    }

    private class TX_MYCD_MBL_L022_RES_DATA{
        private String CARD_REG_CNT = "CARD_REG_CNT";
        private String CARD_MAGR_NM = "CARD_MAGR_NM";
        private String REC = "REC";

    }
}

package com.vannet.mybizcard_rebuild.constant;

public class Constants {

    public static class LoginInfo {
        public static final String USER_ID = "USER_ID";
        public static final String USER_PW = "USER_PW";
        public static final String CHECK_AUTO_LOGIN ="CHECK_AUTO_LOGIN";
        public static final String AUTO_LOGIN = "AUTO_LOGIN";
        public static final String NATION_CD        = "NATION_CD";

    }
}

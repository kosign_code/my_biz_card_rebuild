package com.vannet.mybizcard_rebuild.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.vannet.mybizcard_rebuild.R;
import com.vannet.mybizcard_rebuild.model.CardItem;
import com.vannet.mybizcard_rebuild.tran.ComLoading;
import com.vannet.mybizcard_rebuild.tran.ComTran;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_L022_REC_RES;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_L022_REQ;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_L022_RES;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_P009_REQ;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_P009_RES;
import com.webcash.sws.log.DevLog;

import java.util.ArrayList;
import java.util.HashMap;

public class CardFragment extends Fragment implements View.OnClickListener, ComTran.OnComTranListener {

    ViewPager viewPager;
    public static final String SHARED_PREFS = "sharedPres";
    private SharedPreferences preferences;
    private CardViewPagerAdapter cardViewPagerAdapter;
    private ArrayList<CardItem> cardList;
    private PageIndicatorView pageIndicatorView;
    private ComTran mComTran;
    private ComLoading mLoading;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {

            callApiCard();

        }catch (Exception e){
            e.getMessage();
        }
    }

    private void callApiCard(){
        mComTran = new ComTran(requireContext(), this);
        try {
            TX_MYCD_MBL_L022_REQ requestData = new TX_MYCD_MBL_L022_REQ();
            requestData.setINQ_DT("20240116");
            mComTran.requestData(TX_MYCD_MBL_L022_REQ.TXNO, requestData.getSendMessage(),false);
            DevLog.devLog("MyApiFragment", "Request Data: " + requestData.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onClick(View v) {

    }

    @Override
    public void onTranResponse(String tranCode, Object object) {
        viewPager = getView().findViewById(R.id.viewPager);
        pageIndicatorView = getView().findViewById(R.id.pageIndicatorView);
        try {
            if(tranCode.equals(TX_MYCD_MBL_L022_REQ.TXNO)) {
                TX_MYCD_MBL_L022_RES res = new TX_MYCD_MBL_L022_RES(object);
                TX_MYCD_MBL_L022_REC_RES rec = res.getREC();

                cardList = new ArrayList<>();

                for (int i = 0; i < rec.getLength(); i++) {
                    rec.move(i);
                    cardList.add(new CardItem(
                            rec.getCARD_CORP_CD(),
                            rec.getCARD_NO(),
                            rec.getCARD_ORG_NM(),
                            rec.getBIZ_NM(),
                            rec.getCARD_NICK_NM(),
                            rec.getEXPR_TRM(),
                            rec.getCARD_GB(),
                            rec.getSCRP_YN(),
                            rec.getSEQ_NO()
                    ));

                    DevLog.devLog(">>>> "+ rec.getCARD_NO());
                }
                cardViewPagerAdapter = new CardViewPagerAdapter(getContext(), cardList);
                viewPager.setPadding(100, 0 ,100,0);
                viewPager.setAdapter(cardViewPagerAdapter);
                pageIndicatorView.setViewPager(viewPager);
                pageIndicatorView.setCount(5);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }
}
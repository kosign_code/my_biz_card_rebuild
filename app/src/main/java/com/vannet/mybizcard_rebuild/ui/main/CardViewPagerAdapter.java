package com.vannet.mybizcard_rebuild.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.vannet.mybizcard_rebuild.R;
import com.vannet.mybizcard_rebuild.model.CardItem;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_L022_REC_RES;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_L022_RES;
import com.webcash.sws.log.DevLog;

import org.json.JSONException;

import java.util.ArrayList;

public class CardViewPagerAdapter extends PagerAdapter {

    Context context;
    ArrayList<CardItem> cardList;
    CardItem card;
    TextView textOrgCardName ;
    TextView textBizNm;
    TextView textCardNo;
    TextView textCardNickName;
    ImageView imageView;

    public CardViewPagerAdapter(Context context, ArrayList<CardItem> cardList) {
        this.context = context;
        this.cardList = cardList;
    }
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return cardList.size();
    }
    @NonNull
    @Override
    @SuppressLint({"LocalSuppress", "ResourceType", "MissingInflatedId"})
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_card_fragement,null);
         textOrgCardName = view.findViewById(R.id.txtCARD_ORG_NM);
         textBizNm = view.findViewById(R.id.txtBIZNM);
         textCardNo = view.findViewById(R.id.txtCARD_NO);
         textCardNickName = view.findViewById(R.id.txtNickName);
         imageView = view.findViewById(R.id.imgFragCard);
        try {
            Glide.with(context)
                    .asDrawable()
                    .load(cardList.get(position))
                    .placeholder(R.drawable.white_card)
                    .into(imageView);

            // Set text values
            card = cardList.get(position);

            textOrgCardName.setText(card.getCARD_ORG_NM());
            textCardNickName.setText(card.getCARD_NICK_NM());
            textBizNm.setText(card.getBIZ_NM());
            textCardNo.setText(card.getCARD_NO());
            container.addView(view, 0);
        }catch (Exception e){
            e.getMessage();
        }


        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}

package com.vannet.mybizcard_rebuild.ui.main;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.vannet.mybizcard_rebuild.LoginActivity;
import com.vannet.mybizcard_rebuild.R;

import retrofit2.http.PUT;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager2 viewPager2;
    MainPagerAdapter mainPagerAdapter;

    Toolbar toolbar;

//    private int[] tabIcon = {R.drawable.mn_credit_on_icon, R.drawable.mn_rcpt_on_icon, R.drawable.mn_alarm_on_icon, R.drawable.mn_more_on_icon};

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager2 = findViewById(R.id.viewPager);
        toolbar = findViewById(R.id.toolBar);
        AppCompatActivity activity = this;
        activity.setSupportActionBar(toolbar);
        LayoutInflater inflater = LayoutInflater.from(this);

        mainPagerAdapter = new MainPagerAdapter(this);
        viewPager2.setAdapter(mainPagerAdapter);
        viewPager2.setUserInputEnabled(false);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                try {
                    viewPager2.setCurrentItem(tab.getPosition());
                    if (tab.getPosition()== 0){
                        View customView = inflater.inflate(R.layout.toolbar_card, null);
                        getSupportActionBar().setCustomView(customView);
                        getSupportActionBar().setDisplayShowCustomEnabled(true);
                        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                                ActionBar.LayoutParams.MATCH_PARENT,
                                ActionBar.LayoutParams.MATCH_PARENT);
                        getSupportActionBar().setCustomView(customView, params);
                    } else if (tab.getPosition() == 1) {
                        View customView = inflater.inflate(R.layout.toolbar_receipt, null);
                        getSupportActionBar().setCustomView(customView);
                        getSupportActionBar().setDisplayShowCustomEnabled(true);
                        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                                ActionBar.LayoutParams.MATCH_PARENT,
                                ActionBar.LayoutParams.MATCH_PARENT);
                        getSupportActionBar().setCustomView(customView, params);
                    } else if (tab.getPosition() == 2) {
                        View customView = inflater.inflate(R.layout.toolbar_notifi, null);
                        getSupportActionBar().setCustomView(customView);
                        getSupportActionBar().setDisplayShowCustomEnabled(true);
                        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                                ActionBar.LayoutParams.MATCH_PARENT,
                                ActionBar.LayoutParams.MATCH_PARENT);
                        getSupportActionBar().setCustomView(customView, params);
                    }else {
                        View customView = inflater.inflate(R.layout.toolbar_see_more, null);
                        getSupportActionBar().setCustomView(customView);
                        getSupportActionBar().setDisplayShowCustomEnabled(true);
                        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                                ActionBar.LayoutParams.MATCH_PARENT,
                                ActionBar.LayoutParams.MATCH_PARENT);
                        getSupportActionBar().setCustomView(customView, params);
                    }
                }catch (Exception e){
                    e.getMessage();
                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.getTabAt(position).select();
            }
        });

    }

}
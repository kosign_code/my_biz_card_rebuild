package com.vannet.mybizcard_rebuild.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import com.vannet.mybizcard_rebuild.LoginActivity;
import com.vannet.mybizcard_rebuild.R;
import com.vannet.mybizcard_rebuild.constant.Constants;
import com.vannet.mybizcard_rebuild.tran.ComTran;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_P009_REQ;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_P009_RES;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_USE_INTT_REC;
import com.vannet.mybizcard_rebuild.ui.intro.IntroActivity;
import com.vannet.mybizcard_rebuild.ui.main.MainActivity;
import com.webcash.sws.log.DevLog;
import com.webcash.sws.secure.XorCrypto;

import java.time.Instant;

public class SplashActivity extends AppCompatActivity implements ComTran.OnComTranListener {
    private static int SPLASH_SCREEN = 2500;
    String userId="", userPw="";
    Boolean mCheckAuto;
    ComTran mComTran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(()-> {
            mComTran = new ComTran(this, this);
            SharedPreferences sp = getSharedPreferences(Constants.LoginInfo.AUTO_LOGIN,MODE_PRIVATE);
            userId= sp.getString(Constants.LoginInfo.USER_ID,"");
            userPw = sp.getString(Constants.LoginInfo.USER_PW,"");
            mCheckAuto = sp.getBoolean(Constants.LoginInfo.CHECK_AUTO_LOGIN,false);
            DevLog.devLog("nano>>::"+userId+ userPw + mCheckAuto);

            if (mCheckAuto.equals(true)){
                if (!userId.isEmpty() && !userPw.isEmpty()){
                    callLogin();
                }
            }else {
                Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
                startActivity(intent);
                finish();
            }

        },SPLASH_SCREEN);
    }
    private void callLogin() {
        try{

            TX_MYCD_MBL_P009_REQ req = new TX_MYCD_MBL_P009_REQ();
            req.setUSE_INTT_ID("");
            req.setUSER_ID(userId);
//            req.setUSER_ID("simdemo01t");
            req.setENC_GB("0");
            req.setMOBL_CD("1"); // 1:비플법인카드, 4:비플우리카드, 5:NH소호비즈
            req.setUSER_PW(XorCrypto.encoding(userPw));
//            req.setUSER_PW(XorCrypto.encoding("qwer1234!"));
            req.setSESSION_ID("");
            req.setTOKEN("");
            req.setNATION_CD("840");

            mComTran.requestData(TX_MYCD_MBL_P009_REQ.TXNO, req.getSendMessage(), false);
//            DevLog.devLog("MYCD_MBL: "+ req)


        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onTranResponse(String tranCode, Object object) throws Exception {
        try {
            if(tranCode.equals(TX_MYCD_MBL_P009_REQ.TXNO)) {

                //set cookie 세션
                String bizURL = "https://webank-dev.appplay.co.kr/CardAPI.do";
//                mComTran.setCookie(bizURL);
                TX_MYCD_MBL_P009_RES res = new TX_MYCD_MBL_P009_RES(object);
                DevLog.devLog("MYCD_MBL_P009: "+ res);

                SharedPreferences sharedPreferences = getSharedPreferences(Constants.LoginInfo.AUTO_LOGIN, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Constants.LoginInfo.USER_ID,userId);
                editor.putString(Constants.LoginInfo.USER_PW,userPw);
                editor.apply();

                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onTranError(String tranCode, Object object) {

    }

}
package com.vannet.mybizcard_rebuild.ui.intro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rd.PageIndicatorView;
import com.vannet.mybizcard_rebuild.LoginActivity;
import com.vannet.mybizcard_rebuild.R;
import com.vannet.mybizcard_rebuild.constant.Constants;
import com.vannet.mybizcard_rebuild.tran.ComTran;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_P009_REQ;
import com.vannet.mybizcard_rebuild.tx.TX_MYCD_MBL_P009_RES;
import com.vannet.mybizcard_rebuild.ui.main.MainActivity;
import com.vannet.mybizcard_rebuild.ui.splash.SplashActivity;
import com.webcash.sws.log.DevLog;
import com.webcash.sws.secure.XorCrypto;

import org.w3c.dom.Text;

import java.time.Instant;

public class IntroActivity extends AppCompatActivity {

    ViewPager viewPager;
    Button btnStart;
    IntroPagerAdapter introPagerAdapter;
    PageIndicatorView pageIndicatorView;
    LinearLayout mDotLayout;
    TextView[] dots;
    Intent intent;



    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        btnStart = findViewById(R.id.btnStart);
        viewPager = findViewById(R.id.viewPager);
        mDotLayout = findViewById(R.id.indicator_layout);




            //        pageIndicatorView = findViewById(R.id.pageIndicatorView);

            introPagerAdapter = new IntroPagerAdapter(this);
            viewPager.setAdapter(introPagerAdapter);
            setUpindicator(0);
            viewPager.addOnPageChangeListener(viewListener);
//        pageIndicatorView.setViewPager(viewPager);
//        pageIndicatorView.setCount(6);


            btnStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(IntroActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            });
    }

    public void setUpindicator(int position){
        dots = new TextView[6];
        mDotLayout.removeAllViews();

        for (int i = 0; i<dots.length; i++){
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226"));
            dots[i].setTextSize(30);
            dots[i].setLetterSpacing(0.2F);
            dots[i].setTextColor(getResources().getColor(R.color.app_dis_button_color,getApplicationContext().getTheme()));
            mDotLayout.addView(dots[i]);
        }
        dots[position].setTextColor(getResources().getColor(R.color.highligh_color,getApplicationContext().getTheme()));
    }
ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setUpindicator(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
  };
}
package com.vannet.mybizcard_rebuild.ui.main;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class MainPagerAdapter extends FragmentStateAdapter {


    public MainPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        if (position == 0){
            return new CardFragment();
        } else if (position == 1) {
            return new ReceiptFragment();
        } else if (position == 2) {
            return new NotificationFragment();
        }
        else {
            return new SeeMoreFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }
}

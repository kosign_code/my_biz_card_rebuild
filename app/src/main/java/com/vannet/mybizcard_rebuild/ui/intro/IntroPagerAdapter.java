package com.vannet.mybizcard_rebuild.ui.intro;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.rd.PageIndicatorView;
import com.vannet.mybizcard_rebuild.R;

public class IntroPagerAdapter extends PagerAdapter {

    Context context;

    int bgImage[] = {
          R.drawable.bg_notifi_intro,
          R.drawable.bg_send_receipt_intro,
          R.drawable.bg_save_receipt_intro,
          R.drawable.bg_ex_report_intro,
          R.drawable.bg_analytic_intro,
          R.drawable.bg_cp_card_intro
    };
    int images[] ={
            R.drawable.img_notifi_intro,
            R.drawable.img_send_receipt_intro,
            R.drawable.img_save_receipt_intro,
            R.drawable.img_ex_report_intro,
            R.drawable.img_analytic_intro,
            R.drawable.img_cp_card_intro
    };

    int heading[] = {
            R.string.intro_head_notifi,
            R.string.intro_head_send,
            R.string.intro_head_save,
            R.string.intro_head_ex_report,
            R.string.intro_head_analytic,
            R.string.intro_head_cp_card
    };

    int description[] = {
            R.string.intro_des_notifi,
            R.string.intro_des_send,
            R.string.intro_des_save,
            R.string.intro_des_ex_report,
            R.string.intro_des_analytic,
            R.string.intro_des_cp_card
    };

    public IntroPagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @SuppressLint("MissingInflatedId")
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_intro_layout,container,false);

        ImageView bgImages = view.findViewById(R.id.imgBgIntro);
        ImageView introImage = view.findViewById(R.id.imgViewIntro);
        TextView introHeading = view.findViewById(R.id.txtTitleIntro);
        TextView introDes = view.findViewById(R.id.txtDesIntro);


        bgImages.setImageResource(bgImage[position]);
        introImage.setImageResource(images[position]);
        introHeading.setText(heading[position]);
        introDes.setText(description[position]);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
